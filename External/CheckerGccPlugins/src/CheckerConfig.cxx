/*
 * Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file CheckerGccPlugins/src/CheckerConfig.h
 * @author scott snyder <snyder@bnl.gov>
 * @date May, 2022
 * @brief Very simple config file reader.
 */


#include "CheckerConfig.h"
#include <fstream>
#include <cstring>
#include <cctype>


namespace {


/**
 * @brief Trim blanks / comments from a string.
 * @param s The string to trim.  Modified in-place.
 *
 * Any comment is removed from @c s as are leading/trailing spaces.
 */
void trim (std::string& s)
{
  // Remove comment if present.
  std::string::size_type pos = s.find('#');
  if (pos != std::string::npos) {
    s.erase (pos, std::string::npos);
  }

  // Remove leading spaces.
  for (pos = 0; pos < s.size() && std::isspace(s[pos]); ++pos) {
  }
  s.erase (0, pos);

  // Remove trailing spaces.
  while (!s.empty() && (std::isspace(s.back()) || s.back() == '\n')) {
    s.resize (s.size()-1);
  }
}


} // anonymous namespace


namespace CheckerGccPlugins {


/**
 * @brief Construct configuration from files given in an environment variable.
 * @param envvar Name of the environment variable to read.
 *
 * @c envvar should expand to a colon-separated list of configuration
 * files to read.
 */
CheckerConfig::CheckerConfig (const std::string& envvar)
{
  readViaEnv (envvar);
}


/**
 * @brief Look up a configuration entry.
 * @param key The key to look up.
 *
 * Returns an empty vector if the key is not present in the configuration.
 */
const std::vector<std::string>&
CheckerConfig::operator[] (const std::string& key) const
{
  auto it = m_map.find (key);
  if (it != m_map.end()) return it->second;
  const static std::vector<std::string> empty;
  return empty;
}


/**
 * @brief Read a configuration file from a C++ stream.
 * @param s The stream from which to read.
 */
void CheckerConfig::read (std::istream& in)
{
  std::vector<std::string> data;
  std::string key;
  for (std::string line; std::getline (in, line); ) {
    trim (line);
    if (line.size() >= 2 && line[0] == '[' && line[line.size()-1] == ']') {
      newkey (std::move (key), std::move (data));
      data.clear();
      key = line.substr (1, line.size()-2);
    }
    else if (!line.empty()) {
      data.push_back (line);
    }
  }
  newkey (std::move (key), std::move (data));
}


/**
 * @brief Read a configuration file given a pathname.
 * @param path The name of the file to read.
 */
void CheckerConfig::read (const std::string& path)
{
  if (!path.empty()) {
    std::ifstream input (path);
    if (input.is_open()) {
      read (input);
    }
  }
}


/**
 * @brief Read configuration files given by an environment variable.
 * @param envvar Name of the environment variable to read.
 *
 * @c envvar should expand to a colon-separated list of configuration
 * files to read.
 */
void CheckerConfig::readViaEnv (const std::string& envvar)
{
  char* pathlist = getenv (envvar.c_str());
  if (!pathlist) return;
  char* saveptr = nullptr;
  while (char* path = strtok_r (pathlist, ":", &saveptr)) {
    pathlist = nullptr;
    read (path);
  }
}


/**
 * @brief Dump the configuration.
 * @param s Stream to which to write.
 */
void CheckerConfig::dump (std::ostream& s) const
{
  for (const auto& p : m_map) {
    s << "[" << p.first << "]\n";
    for (const std::string& str : p.second) {
      s << str << "\n";
    }
    s << "\n";
  }
}


/**
 * @brief Add a new list of strings for a key.
 * @param key Key to which to add.
 * @param data List of strings to append to the list for this key.
 */
void CheckerConfig::newkey (std::string&& key, std::vector<std::string>&& data)
{
  std::vector<std::string>& d = m_map[std::move(key)];
  if (d.empty()) {
    d = std::move(data);
  }
  else {
    d.insert (d.end(), data.begin(), data.end());
  }
}


} // namespace CheckerGccPlugins
