# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Wrapper around VecMem's auto-generated CMake configuration, which ensures that
# the runtime environment of VecMem would be set up correctly.
#

# The LCG include(s).
include( LCGFunctions )

# Use the helper macro to do most of the work.
lcg_wrap_find_module( vecmem
   NO_LIBRARY_DIRS
   IMPORTED_TARGETS vecmem::core )
