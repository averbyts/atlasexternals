# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
from flake8_atlas.test.testutils import Flake8Test

class Test(Flake8Test):
   """
   Test Copyright checker
   """
   def test_fileLimit(self):
      source = '#'
      self.assertPass(source, 'ATL902')

   def test_noCopyright(self):
      source = '#'*300    # to get above min file size
      self.assertFail(source, 'ATL902')

   def test_correctCopyright(self):
      source = '#\n'
      source += '# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration\n'
      source += '#'*300
      self.assertPass(source, 'ATL902')

   def test_wrongCopyright(self):
      source = '#\n'
      source += '# Copyright (C) 2002-2019 Jane Doe\n'
      source += '#'*300
      self.assertFail(source, 'ATL902')
