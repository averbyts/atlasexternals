# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# The name of the package:
atlas_subdir( flake8_atlas )

# External(s) needed::
find_package( Python COMPONENTS Interpreter )
find_package( flake8 )
find_package( flake8_bugbear )
find_package( pip )
find_package( libffi )

# Installation directory:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/flake8_atlasBuild" )
set( _stamp "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/flake8_atlas.stamp" )

# Build the package with pip:
file(GLOB _sources CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/flake8_atlas/*.py")
add_custom_command( OUTPUT "${_stamp}"
   COMMAND ${CMAKE_COMMAND} -E touch "${_stamp}"
   COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL PYTHONUSERBASE=${_buildDir}
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh"
   "${PIP_pip_EXECUTABLE}" install --disable-pip-version-check --no-warn-script-location --no-warn-conflicts
   --no-cache-dir --user "${CMAKE_CURRENT_SOURCE_DIR}"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}" "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   DEPENDS ${_sources}
   WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   COMMENT "Building flake8_atlas...")

# Add target and make the package target depend on it:
add_custom_target( flake8_atlas_plugins ALL
   DEPENDS "${_stamp}" )

add_dependencies( Package_flake8_atlas flake8_atlas_plugins )

# Install the package:
install( DIRECTORY "${_buildDir}/"
   DESTINATION .
   USE_SOURCE_PERMISSIONS OPTIONAL )

atlas_install_scripts( bin/flake8_atlas )

# Define the package tests as ctest:
atlas_add_test( plugin
   SCRIPT python -m unittest discover -s "${CMAKE_CURRENT_SOURCE_DIR}"
   POST_EXEC_SCRIPT "# none" )

atlas_add_test( wrapper
   SCRIPT flake8_atlas --isolated --enable-extension ATL902 "${CMAKE_CURRENT_SOURCE_DIR}/test/example.py"
   PROPERTIES WILL_FAIL TRUE
   POST_EXEC_SCRIPT "# none" )
