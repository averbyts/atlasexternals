# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Convenience file for adding the modules from this package into a build.
#

# Greet the user, and remember the current directory/
if( NOT AtlasHIP_FIND_QUIETLY )
   message( STATUS
      "Including AtlasHIP from directory: ${CMAKE_CURRENT_LIST_DIR}" )
endif()
set( ATLASHIP_MODULE_DIR ${CMAKE_CURRENT_LIST_DIR}/modules CACHE STRING
   "Directory holding the AtlasHIP module files" )
mark_as_advanced( ATLASHIP_MODULE_DIR )

# Now add it to the CMake module path.
list( INSERT CMAKE_MODULE_PATH 0 ${ATLASHIP_MODULE_DIR} )
list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )

# Install the modules with the project.
if( "${CMAKE_INSTALL_CMAKEDIR}" STREQUAL "" )
   message( WARNING
      "AtlasHIP should only be used together with/after AtlasCMake" )
endif()
install( FILES ${ATLASHIP_MODULE_DIR}/AtlasCheckLanguage.cmake
               ${ATLASHIP_MODULE_DIR}/CMakeDetermineHIPCompiler.cmake
               ${ATLASHIP_MODULE_DIR}/CMakeHIPCompiler.cmake.in
               ${ATLASHIP_MODULE_DIR}/CMakeHIPInformation.cmake
               ${ATLASHIP_MODULE_DIR}/CMakeTestHIPCompiler.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/ )
install( FILES ${CMAKE_CURRENT_LIST_DIR}/AtlasHIPConfig.cmake
   ${CMAKE_CURRENT_LIST_DIR}/AtlasHIPConfig-version.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/ )
